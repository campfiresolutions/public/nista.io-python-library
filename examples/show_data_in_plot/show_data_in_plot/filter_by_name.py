import matplotlib.pyplot as plt
from structlog import get_logger
from data_point_client.models.get_data_request import GetDataRequest

from nista_library import NistaConnection, NistaDataPoints

from plotter import Plotter

log = get_logger()


def filter_by_name(connection: NistaConnection):
    dataPoints = NistaDataPoints(connection=connection)
    data_point_list = list(dataPoints.get_data_point_list())

    for data_point in data_point_list:
        log.info(data_point)

    # Find Specific Data Points
    filtered_data_points = filter(
        lambda data_point: data_point.name.startswith(
            "Chiller Cooling Power Production"
        ),
        data_point_list,
    )
    for data_point in filtered_data_points:
        request = GetDataRequest(
            window_seconds=600,
            remove_time_zone=True,
        )

        data_point_data = data_point.get_data_point_data(request=request, timeout=90)

        if isinstance(data_point_data, list):
            Plotter.plot(data_point_data)
