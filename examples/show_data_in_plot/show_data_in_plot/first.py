from data_point_client.models.get_data_request import GetDataRequest

from nista_library import KeyringNistaConnection, NistaDataPoint

connection = KeyringNistaConnection(workspace_id="YOUR_WORKSPACE_ID")

data_point_id = "DATA_POINT_ID"
data_point = NistaDataPoint(connection=connection, data_point_id=data_point_id)

request = GetDataRequest(
    window_seconds=600,
    remove_time_zone=True,
)

data_point_data = data_point.get_data_point_data(request=request, timeout=90)

if isinstance(data_point_data, list):
    print(data_point_data[0])