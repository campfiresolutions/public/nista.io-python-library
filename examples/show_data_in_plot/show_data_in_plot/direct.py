from structlog import get_logger
from data_point_client.models.get_data_request import GetDataRequest

from nista_library import NistaConnection, NistaDataPoint

from plotter import Plotter

log = get_logger()


def direct_sample(connection: NistaConnection):
    data_point_id = "DATA_POINT_ID"
    data_point = NistaDataPoint(connection=connection, data_point_id=data_point_id)

    request = GetDataRequest(
        window_seconds=600,
        remove_time_zone=True,
    )

    data_point_data = data_point.get_data_point_data(request=request, timeout=90)

    log.info("Data has been received. Plotting")
    if isinstance(data_point_data, list):
        Plotter.plot(data_point_data)
