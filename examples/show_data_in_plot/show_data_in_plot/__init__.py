import direct
import filter_by_name
import filter_by_unit
from nista_library import KeyringNistaConnection

connection = KeyringNistaConnection(workspace_id="YOUR_WORKSPACE_ID")

filter_by_unit.filter_by_unit(connection)
filter_by_name.filter_by_name(connection)
direct.direct_sample(connection)
