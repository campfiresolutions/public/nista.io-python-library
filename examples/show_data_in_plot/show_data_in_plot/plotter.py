import math
from typing import List, Optional

import matplotlib
import matplotlib.pyplot as plt
import pandas as pd
from matplotlib import ticker
from structlog import get_logger

log = get_logger()


class Plotter:
    @classmethod
    def plot(cls, to_plot: List[pd.DataFrame], label: Optional[str] = "") -> str:
        log.info("Start Plotting")

        matplotlib.use("TkAgg")

        (fig, axis) = plt.subplots()

        axis.xaxis.set_major_locator(ticker.NullLocator())
        axis.yaxis.set_major_locator(ticker.NullLocator())

        log.debug("getting current axes")
        current_axis = plt.gca()

        log.debug("set visibility of x-axis as False")
        xax = current_axis.axes.get_xaxis()
        xax = xax.set_visible(False)

        log.debug("set visibility of y-axis as False")
        yax = current_axis.axes.get_yaxis()
        yax = yax.set_visible(True)

        axis.set_ylabel(label)

        log.debug("plotting a line plot after changing it's width and height")
        fig.set_figwidth(4.13)
        fig.set_figheight(2)

        min_number = 0
        max_number = 0
        for data_frame in to_plot:
            new_min = math.floor(data_frame.min())
            new_max = math.ceil(data_frame.max())

            if new_min < min_number:
                min_number = new_min
            if new_max > max_number:
                max_number = new_max

        distance = math.floor((max_number - min_number) / 5)

        log.debug(
            "Man Max Detected",
            min_number=min_number,
            max_number=max_number,
            distance=distance,
        )

        if distance <= 0:
            distance = 1

        new_list = range(min_number, max_number, distance)
        plt.yticks(new_list)

        for data_frame in to_plot:
            plt.plot(data_frame, color="#000000")

        log.debug("Plotted Lines")

        fig.tight_layout()

        log.debug("Tightened Layout")

        for spine in axis.spines.values():
            spine.set_visible(False)

        plt.show()
