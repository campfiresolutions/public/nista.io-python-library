To us this sample replace "YOUR_WORKSPACE_ID" and "DATA_POINT_ID" strings with actual workspaceID and DataPointId from your app.nista.io workspace.

You can find this information in the URL of your browser:
https://app.nista.io/workspace/{WORKSPACE_ID}/dashboard/datalibrary/datapoint/{DATA_POINT_ID}

```shell
poetry install
poetry run python show_data_in_plot/__init__.py
```
