=================
Examples
=================

------------------------------------------
Show received Data in a plot
------------------------------------------

The most easy way to receive data is to adresse the Datapoint directly and Plot it.
this is a snippet from the examples project

.. code-block:: shell

    poetry new my-nista-client
    cd my-nista-client
    poetry add nista-library
    poetry add structlog
    poetry add matplotlib
    poetry add tk

.. literalinclude:: ../examples/show_data_in_plot/show_data_in_plot/direct.py

------------------------------------------
List DataPoints and filter by Name
------------------------------------------

You can list all DataPoints from your Workspace by querying the API. Use Filter lambda expressions in order to reduce the list to the entries you want.

In this example we use the Name in order to find DataPoints that start with "Chiller Cooling Power Production"

.. literalinclude:: ../examples/show_data_in_plot/show_data_in_plot/filter_by_name.py

------------------------------------------
Filter by Physical Quantity
------------------------------------------

In order to find DataPoints by it's Unit or Physical Quantity the filter query can be extended to load more data for every datapoint.

.. literalinclude:: ../examples/show_data_in_plot/show_data_in_plot/filter_by_unit.py