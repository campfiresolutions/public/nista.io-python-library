=================
Links
=================

------------------------------------------
Website
------------------------------------------
.. only:: markdown

    .. image:: https://app.nista.io/assets/wordmark.svg
        :width: 80
        :alt: nista_logo

`nista.io <https://nista.io>`_

------------------------------------------
Source Code
------------------------------------------
.. only:: markdown

    .. image:: https://about.gitlab.com/images/icons/logos/slp-logo.svg
        :width: 80
        :alt: gitlab logo

`Gitlab <https://gitlab.com/campfiresolutions/public/nista.io-python-library>`_

------------------------------------------
PyPi
------------------------------------------

.. only:: markdown

    .. image:: https://pypi.org/static/images/logo-small.2a411bc6.svg
        :width: 80
        :alt: pypi logo

`PyPi.io <https://pypi.org/project/nista-library/>`_
