<!-- nista_library documentation master file, created by
sphinx-quickstart on Thu May 18 13:55:53 2023.
You can adapt this file completely to your liking, but it should at least
contain the root `toctree` directive. -->
# Welcome to nista_library’s documentation!

# Contents:


* [Tutorial](tutorial.md)


    * [Create new Poetry Project](tutorial.md#create-new-poetry-project)


    * [Add reference to your Project](tutorial.md#add-reference-to-your-project)


    * [Your first DataPoint](tutorial.md#your-first-datapoint)


    * [Run and Login](tutorial.md#run-and-login)


    * [Keystore](tutorial.md#keystore)


* [Examples](examples.md)


    * [Show received Data in a plot](examples.md#show-received-data-in-a-plot)


    * [List DataPoints and filter by Name](examples.md#list-datapoints-and-filter-by-name)


    * [Filter by Physical Quantity](examples.md#filter-by-physical-quantity)


* [nista_library package](nista_library.md)


    * [Submodules](nista_library.md#submodules)


    * [nista_library.nista_connetion module](nista_library.md#module-nista_library.nista_connetion)


        * [`KeyringNistaConnection`](nista_library.md#nista_library.nista_connetion.KeyringNistaConnection)


            * [`KeyringNistaConnection.__init__()`](nista_library.md#nista_library.nista_connetion.KeyringNistaConnection.__init__)


            * [`KeyringNistaConnection.clear_stored_token()`](nista_library.md#nista_library.nista_connetion.KeyringNistaConnection.clear_stored_token)


        * [`NistaConnection`](nista_library.md#nista_library.nista_connetion.NistaConnection)


            * [`NistaConnection.__init__()`](nista_library.md#nista_library.nista_connetion.NistaConnection.__init__)


            * [`NistaConnection.get_access_token()`](nista_library.md#nista_library.nista_connetion.NistaConnection.get_access_token)


            * [`NistaConnection.scope`](nista_library.md#nista_library.nista_connetion.NistaConnection.scope)


        * [`StaticTokenNistaConnection`](nista_library.md#nista_library.nista_connetion.StaticTokenNistaConnection)


            * [`StaticTokenNistaConnection.__init__()`](nista_library.md#nista_library.nista_connetion.StaticTokenNistaConnection.__init__)


    * [nista_library.nista_credential_manager module](nista_library.md#module-nista_library.nista_credential_manager)


        * [`NistaCredentialManager`](nista_library.md#nista_library.nista_credential_manager.NistaCredentialManager)


            * [`NistaCredentialManager.__init__()`](nista_library.md#nista_library.nista_credential_manager.NistaCredentialManager.__init__)


    * [nista_library.nista_data_point module](nista_library.md#module-nista_library.nista_data_point)


        * [`NistaDataPoint`](nista_library.md#nista_library.nista_data_point.NistaDataPoint)


            * [`NistaDataPoint.DATE_FORMAT`](nista_library.md#nista_library.nista_data_point.NistaDataPoint.DATE_FORMAT)


            * [`NistaDataPoint.DATE_NAME`](nista_library.md#nista_library.nista_data_point.NistaDataPoint.DATE_NAME)


            * [`NistaDataPoint.VALUE_NAME`](nista_library.md#nista_library.nista_data_point.NistaDataPoint.VALUE_NAME)


            * [`NistaDataPoint.__init__()`](nista_library.md#nista_library.nista_data_point.NistaDataPoint.__init__)


            * [`NistaDataPoint.append_data_point_data()`](nista_library.md#nista_library.nista_data_point.NistaDataPoint.append_data_point_data)


            * [`NistaDataPoint.append_data_point_result_parts()`](nista_library.md#nista_library.nista_data_point.NistaDataPoint.append_data_point_result_parts)


            * [`NistaDataPoint.create_new()`](nista_library.md#nista_library.nista_data_point.NistaDataPoint.create_new)


            * [`NistaDataPoint.data_point_response`](nista_library.md#nista_library.nista_data_point.NistaDataPoint.data_point_response)


            * [`NistaDataPoint.get_data_point_data()`](nista_library.md#nista_library.nista_data_point.NistaDataPoint.get_data_point_data)


            * [`NistaDataPoint.set_data_point_data()`](nista_library.md#nista_library.nista_data_point.NistaDataPoint.set_data_point_data)


    * [nista_library.nista_data_points module](nista_library.md#module-nista_library.nista_data_points)


        * [`NistaDataPoints`](nista_library.md#nista_library.nista_data_points.NistaDataPoints)


            * [`NistaDataPoints.__init__()`](nista_library.md#nista_library.nista_data_points.NistaDataPoints.__init__)


            * [`NistaDataPoints.get_data_point_list()`](nista_library.md#nista_library.nista_data_points.NistaDataPoints.get_data_point_list)


    * [Module contents](nista_library.md#module-nista_library)


* [Links](links.md)


    * [Website](links.md#website)


    * [Source Code](links.md#source-code)


    * [PyPi](links.md#pypi)


# Indices and tables


* [Index](genindex.md)


* [Module Index](py-modindex.md)


* [Search Page](search.md)
