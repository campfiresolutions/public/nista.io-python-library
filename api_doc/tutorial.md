# Tutorial

## Create new Poetry Project

Navigate to a folder where you want to create your project and type

```shell
poetry new my-nista-client
cd my-nista-client
```

## Add reference to your Project

Navigate to the newly created project and add the PyPI package

```shell
poetry add nista-library
```

## Your first DataPoint

In order to receive your datapoint you need a workspaceID and a dataPointId. This can be retrieved from your browser.


* Navigate to app.nista.io and login


* Browse your DataLibrary and open a DataPoint


* You can extract the information from the URL of your browser: [https://app.nista.io/workspace](https://app.nista.io/workspace)/{WORKSPACE_ID}/dashboard/datalibrary/datapoint/{DATA_POINT_ID}

```default
from data_point_client.models.get_data_request import GetDataRequest

from nista_library import KeyringNistaConnection, NistaDataPoint

connection = KeyringNistaConnection(workspace_id="YOUR_WORKSPACE_ID")

data_point_id = "DATA_POINT_ID"
data_point = NistaDataPoint(connection=connection, data_point_id=data_point_id)

request = GetDataRequest(
    window_seconds=600,
    remove_time_zone=True,
)

data_point_data = data_point.get_data_point_data(request=request, timeout=90)

if isinstance(data_point_data, list):
    print(data_point_data[0])
```

## Run and Login

Run your file in poetry’s virtual environment

```shell
$ poetry install
$ poetry run python demo.py
2021-09-02 14:51.58 [info     ] Authentication has been started.
```

In order to login your browser will be openend. If not please copy the URL from the log message your Browser and follow the Login process. If you don’t want to login for every request, please use a Keystore.

## Keystore

Once you loggedin, the library will try to store your access token in your private keystore. Next time you run your programm, it might request a password to access your keystore again to gain access to nista.io
Please take a look at [Keyring](https://pypi.org/project/keyring/) for details.
